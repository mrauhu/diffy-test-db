<?php

require_once 'config.php';
require_once 'lib/SafePDO.php';
require_once 'lib/Db.php';

echo "<pre>";

echo "Подключимся к БД\n";
Db::connect();

echo "Создадим таблицу\n";
Db::create( 'messages', array( 'id INTEGER PRIMARY KEY', 'message TEXT' ) );

echo "Вставим значения\n";
Db::insert( 'messages', array( NULL, '53s' ) );
Db::insert( 'messages', array( 72, 'x' ) );

echo "Получим значения\n";
print_r( Db::select( 'messages', array( '*' ) ) );

echo "Обновим значение\n";
Db::update( 'messages', 72, array( 'message' => 'y' ) );
print_r( Db::select( 'messages', array( '*' ) ) );

echo "Удалим значение\n";
Db::delete( 'messages', 1);
print_r( Db::select( 'messages', array( '*' ) ) );


echo "Список таблиц в базе\n";
print_r( Db::query( 'SELECT name FROM sqlite_master
			WHERE type="table"
			ORDER BY name;' ) );

echo "Удалим таблицу\n";
Db::delete('messages');
print_r( Db::select( 'messages', array( '*' ) ) );


echo "</pre>";
