# Класс Db — работа с базой данных

В классе используются статические методы:
connect, create, insert, select, update, delete.

По умолчанию создается подключение к базе SQLite в памяти.


## Необходимые файлы для работы

	:::php
		<?php
		require_once 'config.php';	// конфигурация, хранится в переменной $cfg['db']
		require_once 'lib/SafePDO.php';	// Автоматическая ловля исключений PDO
		require_once 'lib/Db.php';	// Обертка для PDO


## Пример использования

	:::php
		<?php
		// Подключимся к БД
		Db::connect();

		// Создадим таблицу messages
		Db::create( 'messages', array( 'id INTEGER PRIMARY KEY', 'message TEXT' ) );

		// Вставим значения
		Db::insert( 'messages', array( NULL, 'a' ) );
		Db::insert( 'messages', array( 72, 'x' ) );

		// Обновим значение
		Db::update( 'messages', 72, array( 'message' => 'y' ) );

		// Удалим значение
		Db::delete( 'messages', 1);

		// Получим значения
		print_r( Db::select( 'messages', array( '*' ) ) );

		// Удалим таблицу
		Db::delete('messages');
