<?php

/**
 * Класс работающий с БД через PDO.
 * <br/>По умолчанию настройки берутся из переменной <i>$cfg['db']</i>,
 * прописанной в config.php.
 */
class Db {

	/**
	 * PDO database handler
	 * @var object 
	 */
	private static $dbh;
	
	/**
	 * Создание списка записей из числового или ассоциативного массива
	 * @param array $array Массив записей
	 * @return string Записи через запятую,
	 * <br/><i>список:</i> value_1, value_2 [, ...]
	 * <br/><i>ассоциативный массив:</i> key_1 = :key_1, key_2 = :key_2 [, ...]
	 */
	private static function create_list( $array ) {
		// Проверка, не ассоацивный ли массив?
		/**
		 * @url http://stackoverflow.com/questions/173400/php-arrays-a-good-way-to-check-if-an-array-is-associative-or-numeric/4254008#4254008
		 */
		$is_numeric = !(bool) count(
						array_filter(
								array_keys( $array ), 'is_string'
						)
		);

		$list = '';
		foreach ( $array as $key => $value ) {
			$list .= ( $is_numeric )
				? $value
				: $key . ' = :' . $key;
			$list .=  ', ';
		}
		return substr( $list, 0, -2 );
	}

	/**
	 * Подключение к БД
	 * @global array $cfg Конфигурация
	 */
	public static function connect() {
		global $cfg;

		self::$dbh = new SafePDO( $cfg[ 'db' ][ 'dsn' ],
						$cfg[ 'db' ][ 'username' ],
						$cfg[ 'db' ][ 'password' ],
						$cfg[ 'db' ][ 'driver_options' ]
		);
	}

	/**
	 * Добавление таблицы в БД
	 * @param string $table Название таблицы
	 * @param array $columns Список столбцов
	 */
	public static function create( $table, $columns ) {
		self::$dbh->exec( 'CREATE TABLE ' . $table . ' ('
				. self::create_list( $columns ) . ')' );
	}

	/**
	 * Выполнение запроса к таблице
	 * @param type $sql SQL-запрос
	 * @return array Результат выполнения запроса 
	 */
	public static function query( $sql ) {
		return self::$dbh
						->query( $sql )
						->fetchAll();
	}

	/**
	 * Добавление строки в таблицу
	 * @param string $table называние таблицы
	 * @param array $values	список значений
	 */
	public static function insert( $table, $values ) {
		// Заполним поля для вставки
		$list_values = substr(
				str_repeat( '?, ', count( $values )
				), 0, -2 );

		// Подготовим запрос
		self::$dbh
				->prepare( 'INSERT INTO ' . $table . ' VALUES ( ' . $list_values . ')' )
				->execute( $values );
	}

	/**
	 * Выборка из таблицы
	 * @param string $table
	 * @param array $columns
	 * @param string $params
	 */
	public static function select( $table, $columns, $params = '' ) {
		return self::query( 'SELECT ' . self::create_list( $columns ) . ' FROM ' . $table . ' ' . $params );
	}

	/**
	 * Обновление значения в БД
	 * @param string $table	Таблица
	 * @param int $id	Идентификатор строки
	 * @param array $values	Массив для обновления (название => значение)
	 */
	public static function update( $table, $id, $values ) {
		$values[ 'id' ] = $id; // Передадим в массив для подготовки запроса
		// Подготовим и выполним запрос
		self::$dbh
				->prepare( 'UPDATE ' . $table . ' SET ' . self::create_list( $values ) . ' WHERE id = :id' )
				->execute( $values );
	}

	/**
	 * Удаление столбца в таблице или таблицы
	 * @param string $table
	 * @param int $id	Идентификатор удаляемого столбца, иначе удаляется вся таблица
	 */
	public static function delete( $table, $id = false ) {
		$sql = 'DELETE FROM ' . $table;
		if ( $id ) {
			self::$dbh
					->prepare( $sql . ' WHERE id = ?')
					->execute( array( $id ) );
			
		} else {
			self::query( $sql );
		}
	}

}
